import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-counterdown',
  templateUrl: './counterdown.component.html',
  styleUrls: ['./counterdown.component.scss']
})
export class CounterdownComponent implements OnInit {

  @Input() init: number = null;
  @Output() onDecrease = new EventEmitter();
  @Output() onComplete = new EventEmitter();

  public counter: number = 0;
  private countdownTimeRef: any = null;
  constructor() { }

  ngOnInit() {
    this.startCountDown();
  }

  startCountDown() {
    if (this.init && this.init > 0) {
      this.counter = this.init;
      this.doCountdown();
    }
  }

  private clearTimeout() {
    if (this.countdownTimeRef) {
      clearTimeout(this.countdownTimeRef)
      this.countdownTimeRef = null;
    }
  }

  doCountdown() {

    this.clearTimeout();

    this.countdownTimeRef = setTimeout(() => {
      this.counter -= 1;
      this.procesCountdown();
    }, 1000);
  }

  procesCountdown() {

    this.onDecrease.next(this.counter);

    if (this.counter == 0) {
      this.onComplete.emit();
      console.log('-counter end--');

    } else {
      this.doCountdown();
    }
  }

}
