import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterdownComponent } from './counterdown.component';

describe('CounterdownComponent', () => {
  let component: CounterdownComponent;
  let fixture: ComponentFixture<CounterdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
