import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  // title = 'experimental';
  // form: FormGroup

  counterProgress: number = 0;
  totalCountDown: number = 15;

  constructor(private fb: FormBuilder) {

  }

  ngOnInit(): void {
    // this.form = this.fb.group({
    //   'name': ['', [Validators.required]],
    //   'apellido': ['', Validators.required]
    // })

    // this.form.get('name').disable();
  }

  updateProgress($event){
    this.counterProgress = (this.totalCountDown - $event) / this.totalCountDown * 100;
  }

  countDownFinish(){
    console.log('countdown finished');
    
  }

}
